macro(add_test_source file)
    set(TEST_SOURCE_FILES ${TEST_SOURCE_FILES} ${CMAKE_CURRENT_LIST_DIR}/${file})
endmacro(add_test_source)

macro(add_source file)
    set(SOURCE_FILES ${SOURCE_FILES} ${CMAKE_CURRENT_LIST_DIR}/${file})
endmacro(add_source)

include_directories(${CMAKE_CURRENT_LIST_DIR})

macro(add_header file)
    set(HEADER_FILES ${HEADER_FILES} ${CMAKE_CURRENT_LIST_DIR}/${file})
endmacro()
